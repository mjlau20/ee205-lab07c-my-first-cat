///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello1.cpp 
/// @version 1.0
///
/// @author Michael Lau <mjlau20@hawaii.edu>
/// @date   02_Mar_2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main() {

   cout << "Hello World!" << endl;

   return 0;

}
